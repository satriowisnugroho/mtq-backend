<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>

<head>
    <title>Login &raquo; Ticket</title>
    <link href="css/style-login.css" rel='stylesheet' type='text/css' />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!--webfonts-->
    <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic|Oswald:400,300,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,300,700,800' rel='stylesheet' type='text/css'>
    <!--//webfonts-->
    <!-- include sweetAlert css and js -->
    <script src="{{ asset('js/sweet-alert.min.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/sweet-alert.css') }}">
</head>
<body>

<!--start-main-->
<div class="login-10">
    <div class="tenth-login">
        <h4>LOGIN FORM</h4>
        <form class="ten" action="{{ url('auth/login') }}" method="POST">
            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
            <li class="cream">
                <i class=" icon10 user10"></i>
                <input type="text" class="text" name="email" placeholder="Username" value="{{ old('email') }}">
            </li>
            <li class="cream">
                <i class=" icon10 lock10"></i>
                <input type="password" name="password" placeholder="Password">
            </li>
            <div class="submit-ten">
                <input type="submit" value="Log in" >
            </div>
        </form>
    </div>
</div>

<!--//End-login-form-->

<script>
    @if(count($errors))

    var text2 = ""
    @foreach($errors->all() as $error)
    text2 += "- {{ $error }}" + "\n"
    @endforeach

    text2 += "{{ Session::get('errorMessage') }}"

    swal({
        title: "Whoooops!",
        text: "" + text2 + "",
        type: "warning"
    });

    @endif
</script>

</body>
</html>