<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests\LoginRequest;

class LoginController extends Controller {

    public function index()
    {
        return view('auth.login');
    }

    public function postLogin(LoginRequest $request)
    {
        $email = \Request::input('email');
        $password = \Request::input('password');

        if (Auth::attempt(['email' => $email, 'password' => $password]))
        {
            return redirect()->intended('/');
        }
        else
        {
            return redirect('login')->withErrors(
                'Kombinasi email dan password salah.'
            );
        }

    }

    public function getLogout()
    {
        Auth::logout();

        return redirect('login');
    }

}