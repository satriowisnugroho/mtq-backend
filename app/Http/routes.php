<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::controller('auth', 'LoginController');
Route::get('login', 'LoginController@index');

Route::group(['middleware' => 'login'], function(){

    Route::get('/', function () {
        return view('layouts.main');
    });

});

