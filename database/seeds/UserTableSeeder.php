<?php
/**
 * Created by PhpStorm.
 * User: Wisnu
 * Date: 07/05/2015
 * Time: 16:12
 */

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UserTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('users')->delete();

        $users = array(
            array(
                'id' => 1,
                'name' => 'Satrio Wisnugroho',
                'email' => 'admin@mtq.com',
                'password' => bcrypt('admin123')
            )
        );

        foreach ($users as $user) {
            App\User::create($user);
        }

    }

}